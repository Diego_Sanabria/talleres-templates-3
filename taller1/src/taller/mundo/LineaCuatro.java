package taller.mundo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int turno;

	
	/**
	 * Nùmero de filas de la matriz
	 */
	private int filas;

	/**
	 * Modela el número de fichas con las cuales se gana el juego
	 */
	private int fichasTriunfo;
	/**
	 * Número columnas de la matriz
	 */
	private int columnas;
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol, int pFichas)
	{
		tablero = new String[pFil][pCol];
		filas = pFil;
		columnas =pCol;
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		fichasTriunfo=pFichas;
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()

	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		
		int columnaAleatoria;

		
		Random rand= new Random();
				
			columnaAleatoria = rand.nextInt((columnas-1))+0;
			while(!registrarJugada(columnaAleatoria)){
				columnaAleatoria = rand.nextInt((columnas-1))+0;
			}	
	}
	
	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		
	
		String simbolo= null;
		Jugador pJugador = null;

		simbolo=jugadores.get(turno).darSimbolo().toString();
					
			for(int i=(filas-1); i>=0; i--){
				if(tablero[i][col].equals("___")){
					
					tablero[i][col]="_"+simbolo+"_";
					terminar(i,col);
					if(!finJuego){
						if(turno==jugadores.size()-1){
							turno=0;
						}else{
							turno++;
						}
						atacante=jugadores.get(turno).darNombre();
					}
					return true;
				}
			}
		
		
		
		return false;
	}
	/**
	 * Método que determina si hay 4 símbolos iguales al recién jugado consecutivos en la misma fila-
	 * @return true si hay 4 símbolos iguales al recién jugado consecutivos, false de lo contrario
	 */
	private boolean cuatroEnFila(int fil, int col){
		boolean conectados=false;
		int contador=0;
		String simbolo = tablero[fil][col].toString();
		for(int i=0;i<columnas;i++){
			if(simbolo.equals(tablero[fil][i].toString())){
				contador++;
				
				if(contador==fichasTriunfo){
					conectados=true;
					finJuego=true;
				}
			}
			else{
				contador=0;
			}
			
		}
		
		return conectados;
	}
	/**
	 * Método que verifica si hay 4 símbolos iguales al recién jugado consecutivos en la columna
	 * @return true si hay 4 símbolos iguales al recién jugado consecutivos, false de lo contrario
	 */
	private boolean cuatroEnColumna(int fil, int col){
		int contador=0;
		boolean conectados=false;
		String simbolo = tablero[fil][col].toString();
		String pSimbolo;
		for(int i=fil;i<filas;i++){
			pSimbolo=tablero[i][col].toString();
			if(simbolo.equals(pSimbolo))
			{
				contador++;
			}else{
				contador=0;
			}
		}
		if(contador==fichasTriunfo){
			conectados=true;
		}
		return conectados;
	}
	/**
	 * Método que verifica si hay 4 símbolos iguales al recién jugado consecutivos en la diagonal
	 * @return true si hay 4 símbolos iguales al recién jugado consecutivos, false de lo contrario
	 */
	private boolean cuatroEnDiagonalHaciaArriba(int fil,int col){
		boolean conectados=false;
		String simbolo=tablero[fil][col].toString();
		String pSimbolo;
		int pFil=fil;
		int pCol=col;
		int contador=0;
		while(pFil<filas-1&&pCol>0){
			pFil++;
			pCol--;
		}
		while(pFil>0&&pCol<columnas){
			pSimbolo=tablero[pFil][pCol].toString();
			if(simbolo.equals(pSimbolo)){
				contador++;
				if(contador==fichasTriunfo){
					conectados=true;
				}
			}else{
				contador=0;
			}
			pFil--;
			pCol++;
		}
		
		return conectados;
	}
	/**
	 * Método que verifica si hay 4 símbolos iguales al recién jugado consecutivos en la diagonal
	 * @return true si hay 4 símbolos iguales al recién jugado consecutivos, false de lo contrario
	 */
	private boolean cuatroEnDiagonalHaciaAbajo(int fil,int col){
		boolean conectados=false;
		String simbolo=tablero[fil][col].toString();
		String pSimbolo;
		int pFil=fil;
		int pCol=col;
		int contador=0;
		while(pFil>0&&pCol>0){
			pFil--;
			pCol--;
		}
		while(pFil<filas&&pCol<columnas){
			pSimbolo=tablero[pFil][pCol].toString();
			if(simbolo.equals(pSimbolo)){
				contador++;
				if(contador==fichasTriunfo){
					conectados=true;
				}
			}else{
				contador=0;
			}
			pFil++;
			pCol++;
		}
		
		return conectados;
	}
	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{

		if(cuatroEnColumna(fil, col)||cuatroEnDiagonalHaciaAbajo(fil, col)
		||cuatroEnDiagonalHaciaArriba(fil, col)||cuatroEnFila(fil, col))
		   { 
			
			finJuego=true; 
		   }
		
		
		return finJuego;
	}



}
