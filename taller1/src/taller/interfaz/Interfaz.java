package taller.interfaz;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	/**
	 * Fichas con las que se gana el juego
	 */
	private int fichasTriunfo;
	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
					
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{	
		ArrayList<Jugador> jugadores= new ArrayList<Jugador>();
		sc= new Scanner (System.in);
		String nombre;
		String simbolo;
		int filas=0;
		int columnas=0;
		
		ArrayList nombres = new ArrayList<>();
		ArrayList simbolos = new ArrayList<>();
			System.out.println("Ingrese el número de fichas con que desea ganar");
			fichasTriunfo=Integer.parseInt(sc.next());
		while(filas<4){
			System.out.println("Ingrese el número de filas (Mínimo 4)");
			filas=Integer.parseInt(sc.next());
		}
			
		while(columnas<4){
			System.out.println("Ingrese el número de columnas (Mínimo 4)");
			columnas=Integer.parseInt(sc.next());
		}
		do{
			System.out.println("Ingrese el nombre del primer jugador");
			nombre = sc.next();
		}while(nombres.contains(nombre));
			nombres.add(nombre);
		do{
			
			System.out.println("Ingrese el simbolo del primer jugador(Sólo 1 letra o 1 número)");
			simbolo = sc.next();
			
		}while(simbolo.length()!=1||simbolos.contains(simbolo));
		simbolo.toString();
		simbolos.add(simbolo);
		jugadores.add(new Jugador(nombre, simbolo));
		do{
			System.out.println("Ingrese el nombre del segundo jugador");
			nombre = sc.next();
		}while(nombres.contains(nombre));
			nombres.add(nombre);
		do{
			System.out.println("Ingrese el simbolo del segundo jugador(Sólo 1 letra o 1 número)");
			simbolo = sc.next();
			
		}while(simbolo.length()!=1||simbolos.contains(simbolo));
		    simbolo.toString();
			simbolos.add(simbolo);
		jugadores.add(new Jugador(nombre, simbolo));
        while(true){
        	
        	System.out.println("1. Ingresar otro jugador");
        	System.out.println("2. Comenzar con los jugadores actuales");
        	int opt=Integer.parseInt(sc.next());
        	if(opt==1){
        		do{
        			System.out.println("Ingrese el nombre del siguiente jugador");
        			nombre = sc.next();
        			
        		}while(nombres.contains(nombre));
        		nombres.add(nombre);
        		do{
        			System.out.println("Ingrese el simbolo del siguiente jugador(Sólo 1 letra o 1 número)");
        			simbolo = sc.next();
        			
        		}while(simbolo.length()!=1||simbolos.contains(simbolo));
        		simbolo.toString();
        		simbolos.add(simbolo);
        		jugadores.add(new Jugador(nombre, simbolo));
        		continue;
        	}
        	if(opt==2){
        		System.out.println("Los jugadores registrados son: ");
        		Iterator<Jugador> it = jugadores.iterator();
        		while(it.hasNext()){
        		Jugador xJugador=it.next();
        		System.out.println(xJugador.darNombre() + "   con el símbolo:    "+ xJugador.darSimbolo());
        		}
        		System.out.println("---------------Empieza el juego---------------");
        		juego= new LineaCuatro(jugadores, filas, columnas,fichasTriunfo);
        		
        		juego();     		
        		
        		
        		System.out.println("¿Desea comenzar un nuevo juego de este mismo modo?");
        		System.out.println("1. Sí");
            	System.out.println("Presione cualquier otra tecla para volver al menú principal");
            	if(Integer.parseInt(sc.next())==1){
                	empezarJuego();
            	}else{            	
        		break;
        		
            	}
        	}else{
        		System.out.println("Opción inválida");
        		continue;
        	}
        	break;
        }
	}
	
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		
		do{
		imprimirTablero();
		System.out.println("Jugador: "+juego.darAtacante()+", Ingrese la columna en que desea jugar");
		
		juego.registrarJugada(Integer.parseInt(sc.next()));

		
		}while(juego.fin()==false);
		if(juego.fin()){
			System.out.println("Ganó el jugador: "+ juego.darAtacante());
			imprimirTablero();
		}
		
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		ArrayList<Jugador> jugadores= new ArrayList<Jugador>();
		sc= new Scanner (System.in);
		String nombre;
		String simbolo;
		int filas=0;
		int columnas=0;
		System.out.println("Ingrese el número de fichas con que desea ganar");
		fichasTriunfo=Integer.parseInt(sc.next());
		while(filas<4){
			System.out.println("Ingrese el número de filas (Mínimo 4)");
			filas=Integer.parseInt(sc.next());
		}
		while(columnas<4){
			System.out.println("Ingrese el número de columnas (Mínimo 4)");
			columnas=Integer.parseInt(sc.next());
		}
		
			System.out.println("Ingrese su nombre ");
			nombre = sc.next();
		
		do{
			
			System.out.println("Ingrese su símbolo (Sólo 1 letra o 1 número)");
			simbolo = sc.next();
			
		}while(simbolo.length()!=1);
		simbolo.toString();
		jugadores.add(new Jugador(nombre, simbolo));
		jugadores.add(new Jugador("Maquina","M"));
		while (true){
			juego= new LineaCuatro(jugadores, filas, columnas,fichasTriunfo);
		
			juegoMaquina();     		
		
		
			System.out.println("¿Desea comenzar un nuevo juego de este mismo modo?");
			System.out.println("1. Sí");
			System.out.println("Presione cualquier otra tecla para volver al menú principal");
			if(Integer.parseInt(sc.next())==1){
				empezarJuego();
			}else{            	
				break;
		
			}
		}
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		do{
			imprimirTablero();
			
			System.out.println("Ingrese su jugada");
			
			juego.registrarJugada(Integer.parseInt(sc.next()));

			juego.registrarJugadaAleatoria();

				
			
			}while(juego.fin()==false);
			if(juego.fin()){
				System.out.println("Ganó el jugador: "+ juego.darAtacante());
				imprimirTablero();
			}
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		String[][] pTablero = juego.darTablero();
		for(int i=0;i<pTablero.length;i++){
			
			for(int j=0; j<pTablero[0].length;j++){
				
				System.out.print(pTablero[i][j] + " ");
				
			}
			System.out.println();
		}
		//TODO
	}
}
